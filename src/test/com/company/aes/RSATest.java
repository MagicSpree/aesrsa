package com.company.aes;

import com.company.RSA;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import javax.crypto.BadPaddingException;
import java.security.KeyPair;
import java.security.PrivateKey;
import java.security.PublicKey;

class RSATest {

    final static String SIMPLE_TEXT = "Simple Text";

    @Test
    public void checkSuccess_whenSimpleEncryptAndDecryptString_With2048SizeKeys() throws Exception {
        RSA rsa = new RSA();
        KeyPair pair = rsa.generationKeyPair(RSA.LengthRSAKey.BITS_2048);
        PrivateKey privateKey = pair.getPrivate();
        PublicKey publicKey = pair.getPublic();

        String encryptedMessageBytes = rsa.encrypt(SIMPLE_TEXT, publicKey);
        String plainText = rsa.decrypt(encryptedMessageBytes, privateKey);

        Assertions.assertEquals(SIMPLE_TEXT, plainText);
    }

    @Test
    public void checkSuccess_whenEncryptAndDecryptString_WithInvalidKey() throws Exception {
        RSA rsa = new RSA();
        KeyPair pair = rsa.generationKeyPair(RSA.LengthRSAKey.BITS_2048);
        PublicKey publicKey = pair.getPublic();

        pair = rsa.generationKeyPair(RSA.LengthRSAKey.BITS_2048);
        PrivateKey privateKey = pair.getPrivate();

        String encryptedMessageBytes = rsa.encrypt(SIMPLE_TEXT, publicKey);

        BadPaddingException badPaddingException = Assertions.assertThrows(BadPaddingException.class,
                () -> rsa.decrypt(encryptedMessageBytes, privateKey)
        );

        Assertions.assertTrue(badPaddingException.getMessage().contains("Decryption error"));
    }

    @Test
    public void checkSuccess_whenSimpleEncryptAndDecryptString_With1024SizeKeys() throws Exception {
        RSA rsa = new RSA();
        KeyPair pair = rsa.generationKeyPair(RSA.LengthRSAKey.BITS_1024);
        PrivateKey privateKey = pair.getPrivate();
        PublicKey publicKey = pair.getPublic();

        String encryptedMessageBytes = rsa.encrypt(SIMPLE_TEXT, publicKey);
        String plainText = rsa.decrypt(encryptedMessageBytes, privateKey);

        Assertions.assertEquals(SIMPLE_TEXT, plainText);
    }

    @Test
    public void checkSuccess_whenSimpleEncryptAndDecryptString_With4096SizeKeys() throws Exception {
        RSA rsa = new RSA();
        KeyPair pair = rsa.generationKeyPair(RSA.LengthRSAKey.BITS_4096);
        PrivateKey privateKey = pair.getPrivate();
        PublicKey publicKey = pair.getPublic();

        String encryptedMessageBytes = rsa.encrypt(SIMPLE_TEXT, publicKey);
        String plainText = rsa.decrypt(encryptedMessageBytes, privateKey);

        Assertions.assertEquals(SIMPLE_TEXT, plainText);
    }
}