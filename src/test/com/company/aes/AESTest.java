package com.company.aes;

import com.company.AES;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import javax.crypto.BadPaddingException;
import javax.crypto.spec.GCMParameterSpec;


class AESTest {

    private AES aes = new AES();

    private final static String SIMPLE_TEXT = "Simple Text";
    private final static String PASSWORD = "Simple Key";

    private final static String ALGORITHM_ECB = "AES/ECB/PKCS5Padding";
    private final static String ALGORITHM_CBC = "AES/CBC/PKCS5Padding";
    private final static String ALGORITHM_CTR = "AES/CTR/PKCS5Padding";
    private final static String ALGORITHM_GCM = "AES/GCM/PKCS5Padding";

    @Test
    public void checkSuccess_WhenCBC() throws Exception {

        var key = aes.getKeyFromPassword(PASSWORD);
        var IV = aes.generateIV();

        String cipherText = aes.encrypt(ALGORITHM_CBC, SIMPLE_TEXT, key, IV);
        String plainText = aes.decrypt(ALGORITHM_CBC, cipherText, key, IV);

        Assertions.assertEquals(SIMPLE_TEXT, plainText);
    }

    @Test
    public void checkSuccess_WhenECB() throws Exception{

        var key = aes.getKeyFromPassword(PASSWORD);

        String cipherText = aes.encrypt(ALGORITHM_ECB, SIMPLE_TEXT, key);
        String plainText = aes.decrypt(ALGORITHM_ECB, cipherText, key);

        Assertions.assertEquals(SIMPLE_TEXT, plainText);
    }

    @Test
    public void checkSuccess_WhenCTR() throws Exception {

        var IV = aes.generateIV();
        var key = aes.getKeyFromPassword(PASSWORD);

        String cipherText = aes.encrypt(ALGORITHM_CTR, SIMPLE_TEXT, key, IV);
        String plainText = aes.decrypt(ALGORITHM_CTR, cipherText, key, IV);

        Assertions.assertEquals(SIMPLE_TEXT, plainText);
    }

    @Test
    public void checkSuccess_WhenGCM() throws Exception{

        var IV = aes.generateIV();
        var key = aes.getKeyFromPassword(PASSWORD);
        GCMParameterSpec gcmParameterSpec = new GCMParameterSpec(96, IV.getIV());

        String cipherText = aes.encrypt(ALGORITHM_GCM, SIMPLE_TEXT, key, gcmParameterSpec);
        String plainText = aes.decrypt(ALGORITHM_GCM, cipherText, key, gcmParameterSpec);

        Assertions.assertEquals(SIMPLE_TEXT, plainText);
    }

    @Test
    public void checkSuccess_WhenKeyGeneration() throws Exception {

        aes = new AES(AES.LengthAESKey.BITS_192);
        var key = aes.getKey();
        var IV = aes.generateIV();

        String cipherText = aes.encrypt(ALGORITHM_CBC, SIMPLE_TEXT, key, IV);
        String plainText = aes.decrypt(ALGORITHM_CBC, cipherText, key, IV);

        Assertions.assertEquals(SIMPLE_TEXT, plainText);
    }

    @Test
    public void checkError_WhenInvalidPassword() throws Exception{

        aes = new AES(AES.LengthAESKey.BITS_192);
        var key = aes.getKeyFromPassword(PASSWORD);
        var IV = aes.generateIV();

        String cipherText = aes.encrypt(ALGORITHM_CBC, SIMPLE_TEXT, key, IV);
        var newPassword = aes.getKeyFromPassword("PASSWORD");

        BadPaddingException badPaddingException = Assertions.assertThrows(BadPaddingException.class,
                () -> aes.decrypt(ALGORITHM_CBC, cipherText, newPassword, IV)
        );

        Assertions.assertTrue(badPaddingException.getMessage().contains("bad key"));
    }

}