package com.company;

import javax.crypto.*;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.spec.SecretKeySpec;
import java.security.*;
import java.security.spec.AlgorithmParameterSpec;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.KeySpec;
import java.util.Arrays;
import java.util.Base64;

public class AES {

    private Cipher cipher;
    private SecretKey key;

    public AES(LengthAESKey keyLength) throws NoSuchAlgorithmException {
        key = generationKey(keyLength.getLength());
    }

    public AES() {

    }

    private SecretKey generationKey(int keyLength) throws NoSuchAlgorithmException {
        KeyGenerator keyGenerator = KeyGenerator.getInstance("AES");
        keyGenerator.init(keyLength);
        return keyGenerator.generateKey();
    }

    public SecretKey getKeyFromPassword(String password)
            throws InvalidKeySpecException, NoSuchAlgorithmException {

        byte[] salt = generateSalt();
        SecretKeyFactory factory = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA256");
        KeySpec spec = new PBEKeySpec(password.toCharArray(), salt, 65536, 256);
        return new SecretKeySpec(factory.generateSecret(spec)
                .getEncoded(), "AES");
    }

    private byte[] generateSalt() {
        byte[] bytes = new byte[20];
        new SecureRandom().nextBytes(bytes);
        return bytes;
    }

    public IvParameterSpec generateIV() {
        byte[] iv = new byte[16];
        new SecureRandom().nextBytes(iv);
        return new IvParameterSpec(iv);
    }

    public String encrypt(String algorithm, String plainText, SecretKey key, AlgorithmParameterSpec parameter)
            throws Exception {

        cipher = Cipher.getInstance(algorithm);
        cipher.init(Cipher.ENCRYPT_MODE, key, parameter);
        return encryptEngine(plainText);
    }

    public String decrypt(String algorithm, String cipherText, SecretKey key, AlgorithmParameterSpec parameter)
            throws Exception{

        cipher = Cipher.getInstance(algorithm);
        cipher.init(Cipher.DECRYPT_MODE, key, parameter);
        return decryptEngine(cipherText);
    }

    public String encrypt(String algorithm, String plainText, SecretKey key) throws Exception {

        cipher = Cipher.getInstance(algorithm);
        cipher.init(Cipher.ENCRYPT_MODE, key);
        return encryptEngine(plainText);
    }

    public String decrypt(String algorithm, String cipherText, SecretKey key) throws Exception {

        cipher = Cipher.getInstance(algorithm);
        cipher.init(Cipher.DECRYPT_MODE, key);
        return decryptEngine(cipherText);
    }

    private String encryptEngine(String plainText) throws Exception {
        byte[] cipherText = cipher.doFinal(plainText.getBytes());
        return Base64.getEncoder()
                .encodeToString(cipherText);
    }

    private String decryptEngine(String cipherText) throws Exception {
        byte[] plainText = cipher.doFinal(Base64.getDecoder()
                .decode(cipherText));
        return new String(plainText);
    }

    public SecretKey getKey() {
        return key;
    }

    public enum LengthAESKey {
        BITS_128(128),
        BITS_192(192),
        BITS_256(256);

        private final int length;

        LengthAESKey(int length) {
            this.length = length;
        }

        public int getLength() {
            return length;
        }
    }
}
