package com.company;

import javax.crypto.Cipher;
import java.nio.charset.StandardCharsets;
import java.security.*;
import java.util.Base64;

public class RSA {

    final static String ALGORITHM = "RSA/ECB/OAEPWithSHA-256AndMGF1Padding";

    public KeyPair generationKeyPair(LengthRSAKey lengthRSAKey) throws NoSuchAlgorithmException {
        KeyPairGenerator generator = KeyPairGenerator.getInstance("RSA");
        generator.initialize(lengthRSAKey.getLength());
        return generator.generateKeyPair();
    }

    public String encrypt(String input, PublicKey publicKey) throws Exception {
        Cipher cipher = Cipher.getInstance(ALGORITHM);
        cipher.init(Cipher.ENCRYPT_MODE, publicKey);
        byte[] cipherText = cipher.doFinal(input.getBytes(StandardCharsets.UTF_8));
        return Base64.getEncoder()
                .encodeToString(cipherText);
    }

    public String decrypt(String cipherText, PrivateKey privateKey) throws Exception {
        Cipher cipher = Cipher.getInstance(ALGORITHM);
        cipher.init(Cipher.DECRYPT_MODE, privateKey);
        byte[] plainText = cipher.doFinal(Base64.getDecoder()
                .decode(cipherText));
        return new String(plainText);
    }

    public enum LengthRSAKey {
        BITS_1024(1024),
        BITS_2048(2048),
        BITS_4096(4096);

        private final int length;

        LengthRSAKey(int length) {
            this.length = length;
        }

        public int getLength() {
            return length;
        }
    }
}
